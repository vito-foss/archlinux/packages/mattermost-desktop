#!/usr/bin/env bash

set -euo pipefail

conf_file="${XDG_CONFIG_HOME:-"$HOME/.config"}/Mattermost/mattermost-flags.conf"

declare -a flags

if [[ -f "${conf_file}" ]]; then
  mapfile -t conf_list <"${conf_file}"
fi

for line in "${conf_list[@]}"; do
  if [[ ! "${line}" =~ ^[[:space:]]*(#|$) ]]; then
    flags+=("${line}")
  fi
done

exec @ELECTRON@ /usr/lib/mattermost-desktop/app.asar --disableDevMode "${flags[@]}" "$@"
