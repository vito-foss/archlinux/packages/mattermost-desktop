# Maintainer: Caleb Maclennan <caleb@alerque.com>
# Maintainer: Bruno Pagani <archange@archlinux.org>
# Contributor: Raphael Nestler <archlinux@rnstlr.ch>
# Contributor: William Gathoye <william + aur at gathoye dot be>
# Contributor: Aleksandar Trifunović <akstrfn at gmail dot com>
# Contributor: Jan Was <janek dot jan at gmail dot com>
# Contributor: AUR[Severus]

pkgname=mattermost-desktop
pkgver=5.11.0
pkgrel=1
pkgdesc='Mattermost Desktop application'
arch=(x86_64)
url="https://github.com/mattermost/desktop"
license=(Apache-2.0)
_electron=electron34
depends=("$_electron")
makedepends=(nodejs-lts-jod npm python)

source=("$pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz"
        "$pkgname.png"
        "$pkgname.sh"
        'mattermost.desktop'
        'electron-builder-targets.patch'
        'webpack-version.patch'
        'electron-version.patch'
        'excess-js-dependencies.patch')
sha256sums=('4e6cbfe2e513505354f779f5994e6185fe5dc487f19487dc1c59b2bfa77f4459'
            '54ee01489fc5d3878e38d5298582be0cac5fee865adcdfc4d74b754205dd9470'
            '282a4870d5ee7bf86451ac83d84ddd5f6f181f52e092bfd344a492d4f38f18dc'
            '27c798af15d6aefbd41da5aacb9f9b15438349c649e9c05658387981760fd3b7'
            '73165b92f95e87909977d02fffd113647eba1c1377cc1c07f1a83c4c541ed5d4'
            'e96c1f2749203c5e575cfdaf292d8934d08d9fe2fadaa8b25904bb861a40de56'
            '2814f277aae00f0061a01e3f11b0a98fa1d48b7a07a06b8b503e47fe97d77622'
            '926c9df09a1fca4eef67c0bcfbd5e0e799afab0ff7f5d1bf13e7d5c4ad806a68')

prepare() {
  _npmargs=(--cache "$srcdir/npm-cache" --no-audit --no-fund)

  sed "s/@ELECTRON@/$_electron/" -i "$pkgname.sh"

  cd "desktop-$pkgver"

  patch -Np1 -i "$srcdir"/electron-builder-targets.patch
  patch -Np1 -i "$srcdir"/webpack-version.patch
  patch -Np1 -i "$srcdir"/electron-version.patch
  patch -Np1 -i "$srcdir"/excess-js-dependencies.patch

  electronDist="/usr/lib/$_electron"
  electronVersion="$(<"$electronDist/version")"
  sed -e "s|@ELECTRON_DIST@|$electronDist|" -e "s/@ELECTRON_VERSION@/$electronVersion/" -i electron-builder.json
  sed "s/@ELECTRON_VERSION@/$electronVersion/" -i package.json

  npm "${_npmargs[@]}" ci
}

build() {
  cd "desktop-$pkgver"

  npm "${_npmargs[@]}" --offline run package:linux
}

package() {
  cd "desktop-$pkgver"
  install -Dm0644 release/linux-unpacked/resources/app.asar "$pkgdir/usr/lib/$pkgname/app.asar"
  cp -a release/linux-unpacked/resources/app.asar.unpacked "$pkgdir/usr/lib/$pkgname/"
  install -Dm0644 LICENSE.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"
  install -Dm0644 "$srcdir/$pkgname.png" "$pkgdir/usr/share/icons/hicolor/512x512/apps/$pkgname.png"
  install -Dm0755 "$srcdir/$pkgname.sh" "$pkgdir/usr/bin/$pkgname"
  install -Dm0644 "$srcdir/mattermost.desktop" "$pkgdir/usr/share/applications/mattermost.desktop"
}
